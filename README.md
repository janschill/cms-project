# Content-Management-Systeme Semesterprojekt
Umsetzung einer komplexen Webseite mit Drupal für die MS Seeadler.

## VM Konfigurieren
Um vernünftig auf VM Instanz arbeiten zu können, hinterlegen wir anfangs unsere SSH Public Keys auf dem Server.
Dafür kopieren wir unsere lokalen Public Keys:
`pbcopy < ~/.ssh/id_rsa.pub`

Nun verbinden wir uns mit dem Server per SSH `ssh benutzername@serveradresse` und bestätigen den Zugriff mit dem Passwort.

Sind wir mit dem Server verbunden, erstellen wir eine Datei `authorized_keys` im Ordner `.ssh` im Root-Verzeichnis. Dort fügen wir die SSH Public Keys der Personen/Rechner, die sich mit dem Server verbinden sollen, ein.

### Server shortcuts 

#### Services

- `sudo service --status-all`
- `sudo service nginx restart`
- `sudo service php restart`

## Deployment

Simples Deployment, bzw. das Versenden von Daten auf den Server erfolgt durch `rsync`, `wget` oder `scp`. Bauen wir uns unser Kommando zusammen, bekommen wir:

`rsync -av -e "ssh" index.html benutzername@serveradresse:/var/www/cmsha/web/`

Hiermit schicken wir vom aktuellen Verzeichnis, wo das Kommando ausgeführt wird, eine Datein namens `index.html` in das Verzeichnis: `/var/www/cmsha/web/`

### Lokal Installation hochladen

Wir installieren Drupal lokal und konfigurieren die Installation mit Sqlite um ein gemeinsames Arbeiten am Projekt einfacher zu gestalten. Danach zippen wir das Projekt, stellen eine VPN Verbindung zum Hochschulenetz auf und kopieren die gezippte Installation auf den Server.

Verbindung zum Server aufbauen
`ssh p123456@p123456.mittwald.info`
Dateien zippen
`tar -cvzf newfilename.tar.gz file`
Dateien von lokal zu remote laden
`wget url/newfilename.tar.gz`
Dateien unzippen
`tar -xvzf newfilename.tar.gz`

## Konzeption der Webseite

### Was wir vom Kunden brauchen

* hochauflösende Bilder: 
    * Schiffe, Ausflugsziele (Halligen, Watt), alles auf Website
    * Einverständndis
    * Grafik Sitzplan?
    * Vektorgrafiken, Logos

### Features

* Fahrten Buchen
* Platzwahl
* Liste von Fahrten
* Ausflugsziele
* Schiff für Extrafahrten reservieren (z.B. Geburtstag)
* Wattwanderungen reservieren
    * Dauer, Personen, Ziel
    
### Aufbau
 * Startseite: Einführung, nächste Fahrten
 * Fahrten & Buchung
 * Ausflugsziele
 * Spezielle Angebote
 * MS Seeadler
 * Kontakt
 
