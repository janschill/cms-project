var gulp = require('gulp'),
  // eslint = require('gulp-eslint'),
  uglify = require('gulp-uglify'),
  concat = require('gulp-concat'),
  sass = require('gulp-sass'),
  rename = require('gulp-rename'),
  // stylelint = require('gulp-stylelint'),
  autoprefixer = require('gulp-autoprefixer');

// Function that catch errors and will therefore prevent gulp to exit the watch task as long as an on listener is set.
function swallowError(error) {
  console.log(error.messageFormatted);
}

gulp.task('watch', function() {
  gulp.watch('web/themes/custom/customtheme/assets/sass/**/*.scss', ['css']);
  gulp.watch('web/themes/custom/customtheme/assets/js/**/*.js', ['uglify']);
});

// gulp.task('eslint', function() {
//   return gulp
//     .src(['web/themes/custom/customtheme/assets/js/**/*.js'])
//     .pipe(eslint())
//     .pipe(eslint.format())
//     .pipe(eslint.failAfterError());
// });

gulp.task('uglify', function() {
  gulp
    .src(['web/themes/custom/customtheme/assets/js/**/*.js'])
    .pipe(concat('main.min.js'))
    .pipe(
      uglify({
        mangle: true,
        compress: true
      })
    )
    .on('error', swallowError)
    .pipe(gulp.dest('web/themes/custom/customtheme/js'));
});

gulp.task('html5shiv', function() {
  return gulp
    .src(['node_modules/html5shiv/dist/html5shiv.min.js'])
    .pipe(gulp.dest('web/themes/custom/customtheme/js'));
});

// gulp.task('stylelint', function() {
//   return gulp.src('web/themes/custom/customtheme/assets/sass/**/*.scss').pipe(
//     stylelint({
//       reporters: [
//         {
//           formatter: 'string',
//           console: true
//         }
//       ]
//     })
//   );
// });

gulp.task('css', function() {
  gulp
    .src('web/themes/custom/customtheme/assets/sass/main.scss')
    .pipe(
      sass({
        outputStyle: 'compressed'
      })
    )
    .on('error', swallowError)
    .pipe(
      autoprefixer({
        browsers: ['last 4 versions', '> 5%']
      })
    )
    .pipe(rename('main.min.css'))
    .pipe(gulp.dest('web/themes/custom/customtheme/css'));
});

gulp.task('default', ['uglify', 'html5shiv', 'css']);